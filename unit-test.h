// Distributed under the MIT license
/**
 * @file unit_test.h
 * @author sb.kang@outlook.com
 * @date 09 April 2019
 */

#pragma once
#include <stdio.h>   // printf
#include <stdlib.h>  // exit
#include <string.h>  // memcmp
static int unit_test_counter = 0;

#define TEST_CASE(test)                                        \
  do {                                                         \
    unit_test_counter++;                                       \
    printf("[TEST_CASE(%d): %s]\n", unit_test_counter, #test); \
    test();                                                    \
  } while (0)

#define AUTO_TEST_CASE(test) \
  do {                       \
    if (!(test)) {           \
      printf(#test);         \
      exit(1);               \
    }                        \
  } while (0)

#define AUTO_TEST_CASE_ARRAY_EQUAL(l, r, size)                                \
  do {                                                                        \
    for (size_t i = 0; i < size; i++) {                                       \
      if (l[i] != r[i]) {                                                     \
        printf(" - err: data not equal: (%d) %02X != %02X\n", i, l[i], r[i]); \
        exit(1);                                                              \
      }                                                                       \
    }                                                                         \
  } while (0)

#define AUTO_TEST_CASE_ARRAY_NOT_EQUAL(l, r)                                 \
  do {                                                                       \
    if (sizeof(l) == sizeof(r)) {                                            \
      printf(" - err: size is matched: %s(%d) != %s(%d)", #l, sizeof(l), #r, \
             sizeof(r));                                                     \
      exit(1);                                                               \
    }                                                                        \
  } while (0)

#define AUTO_TEST_CASE_STR_EQUAL(l, r)                           \
  do {                                                           \
    if (!strcmp(l, r)) {                                         \
    } else {                                                     \
      printf(" - err: string is not matched: %s != %s", #l, #r); \
      exit(1);                                                   \
    }                                                            \
  } while (0)
